# Binar Car Management

Car Management Dasboard for Binar Car Rental Website

## Getting started

# Cloning Project

Buat repository folder pada local PC/Leptop, lalu klik kanan > Open With Visual Studio Code.
Buka Terminal bash git lalu copy & paste perintah:

```
git clone git@gitlab.com:Tsucie/binar-car-management.git
```

**Note**
Pastikan sudah membuat SSH key dan menambahkannya pada akun gitlab.
[Cara membuat SSH key](https://gitlab.com/help/user/ssh.md)

# Installation

Setelah membuka projek dengan Visual Studio Code, buka terminal (Powershell) dengan menekan tombol **CTRL + `**. Lalu copy & paste perintah:

```
npm i --save
```

# Run Project

Ketik perintah:

```
npm run dev
```

Untuk menjalankan projek menggunakan nodemon untuk development (Hot Reload).

Ketik perintah:

```
npm start
```

Untuk menjalankan projek.

# Tugas

Arsitektur
- Design ERD (Adji)
- Project Config (Adji)
- Migrasi Table (Rizal)
- Layout Header & Footer (amel)
- Layout Sidebar (Isra)

Fitur (Backend & Frontend)
- Fitur GetList Mobil (Adji)
- Fitur Filter Mobil (dandi)
- Fitur Add Mobil (Rizal)
- Fitur Update Mobil (amel)
- Fitur Delete Mobil (Isra)

# ERD

![bcr_management ERD](./dev/bcr_management.png)