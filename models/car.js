"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Car extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Car.init(
    {
      size_id: DataTypes.STRING,
      name: DataTypes.STRING,
      price: DataTypes.DECIMAL,
      photo: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Car",
    }
  );
  Car.associate = function (models) {
    // associations can be defined here

    Car.belongsTo(models.Size, {
      foreignKey: "size_id",
      as: "size",
    });
  };
  return Car;
};

// Relationship between Size and Car
