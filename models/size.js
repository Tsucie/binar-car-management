"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Size extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Size.init(
    {
      name: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "Size",
    }
  );
  Size.associate = function (models) {
    // associations can be defined here
    Size.hasMany(models.Car, {
      foreignKey: "size_id",
      as: "users",
    });
  };
  return Size;
};

//Relationship between Size and Car
