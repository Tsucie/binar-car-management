var sidebar = "1";

$(document).ready(function () {
  getCars();
  if (typeof(Storage) !== "undefined") {
    if (localStorage.getItem("response")) {
      pesanAlert(JSON.parse(localStorage.getItem("response")));
      setTimeout(() => {
        localStorage.removeItem("response");
      }, 1000);
    }
  }
  if (localStorage.getItem('edit_id')) {
    getCarByID(parseInt(localStorage.getItem('edit_id')));
    localStorage.removeItem('edit_id');
  }
});

function getCars(filter = "") {
  let container = $("#cars-container");
  let param = new Object();
  if (filter) param.filter = filter;
  $.ajax({
    url: "/api/car/getAll",
    type: "POST",
    timeout: 0,
    data: JSON.stringify(param),
    headers: {
      "Content-Type": "application/json",
    },
    success: function (response) {
      if (response.code == 200) {
        if (response.data.length > 0) {
          container.empty();
          let rowHtml = "";
          response.data.forEach((e) => {
            rowHtml =
              '<div class="col-lg-4 py-3">' +
              '<div class="card" style="object-fit: cover;">' +
              '<div class="image-container">' +
              '<img class="card-img-top" src="/img/' +
              e.photo +
              '" alt="' +
              e.photo +
              '">' +
              "</div>" +
              '<div class="card-body">' +
              "<p>" +
              e.name +
              "/" +
              size(e.size_id) +
              "</p>" +
              '<h6 class="card-title"><strong>Rp ' +
              e.price +
              " / hari</strong></h6>" +
              '<p><span><img src="/icon/fi_clock.png" /></span>&nbsp;&nbsp;Updated at ' +
              dateFormater(e.updatedAt) +
              "</p>" +
              '<div class="card-buttons">' +
              '<button class="btn-middle btn-delete" id="delete-car" onclick="deleteCar('+e.id+')"><span><img src="/icon/fi_trash.png" /></span>&nbsp;&nbsp; Delete</button>' +
              '<button class="btn-middle btn-edit" id="edit-car" onclick="redirectToEdit(' +
              e.id +
              ')"><span><img src="/icon/fi_edit.png" /></span>&nbsp;&nbsp; Edit</button>' +
              "</div>" +
              "</div>" +
              "</div>" +
              "</div>";
            container.append(rowHtml);
          });
        } else {
          container.empty();
          container.append('<h4 class="text-center">Tidak ada Data</h4>');
        }
      } else {
        alert(`${response.code}: ${response.message}`);
      }
    },
    error: function (jqXHR) {
      pesanAlert(jqXHR.responseJSON);
    },
  });
}

function getCarByID(id) {
  $.ajax({
    url: "/api/car/getById",
    type: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    data: JSON.stringify({
      "id": id
    }),
    success: function (response) {
      if (response.code == 200) {
        $('#id').val(response.data.id);
        $('#name').val(response.data.name);
        $('#price').val(response.data.price);
        $('#size_id').val(response.data.size_id);
        $('#createdAt').text(dateFormater(response.data.createdAt));
        $('#updatedAt').text(dateFormater(response.data.updatedAt));
      }
      else pesanAlert(response);
    },
    error: function (jqXHR) {
      pesanAlert(jqXHR.responseJSON);
    },
  });
}

function addCar() {
  DisableBtn("#btn_add_car");
  var formData = new FormData();
  formData.append("name", $("#name").val());
  formData.append("price", $("#price").val());
  formData.append("size_id", $("#size").val());
  formData.append("photo", $("#photo")[0].files[0]);

  $.ajax({
    url: "/api/car/add",
    type: "POST",
    data: formData,
    processData: false,
    contentType: false,
    success: function (response) {
      if (response.code == 201) {
        if (typeof(Storage) !== "undefined") {
          localStorage.setItem("response", JSON.stringify(response));
        }
        window.location.href = "/cars";
      }
      else pesanAlert(response);
    },
    error: function (jqXHR) {
      pesanAlert(jqXHR.responseJSON);
    },
    complete: function () {
      EnableBtn("#btn_add_car", "Save");
    },
  });
}

function editCar() {
  DisableBtn("#btn_edit_car");
  var formData = new FormData();
  formData.append("id", $('#id').val());
  formData.append("name", $("#name").val());
  formData.append("price", $("#price").val());
  formData.append("size_id", $("#size_id").val());
  if ($('#photo')[0].files[0]) {
    console.log($('#photo')[0].files[0]);
    formData.append("photo", $("#photo")[0].files[0]);
  }

  $.ajax({
    url: '/api/car/edit',
    type: "PUT",
    data: formData,
    processData: false,
    contentType: false,
    success: function (response) {
      if (response.code == 201) {
        if (typeof(Storage) !== "undefined") {
          localStorage.setItem("response", JSON.stringify(response));
        }
        window.location.href = "/cars";
      }
      else pesanAlert(response);
    },
    error: function (jqXHR) {
      pesanAlert(jqXHR.responseJSON);
    },
    complete: function () {
      EnableBtn("#btn_edit_car", "Save");
    },
  });
}

function deleteCar(id) {
  $('#btn-hps').attr('data_id', id);
  $('#modal-delete').modal('show');
}

function destroyCar(obj) {
  DisableBtn("#btn-hps");
  let id = { "id": parseInt(obj.attributes.data_id.value) };
  $.ajax({
    url: "/api/car/delete",
    type: "DELETE",
    headers: {
      "Content-Type": "application/json"
    },
    data: JSON.stringify(id),
    success: function (response) {
      if (response.code == 202) {
        if (typeof(Storage) !== "undefined") {
          localStorage.setItem("response", JSON.stringify(response));
        }
        window.location.reload();
      }
      else pesanAlert(response);
    },
    error: function (jqXHR) {
      pesanAlert(jqXHR.responseJSON);
    },
    complete: function () {
      EnableBtn("#btn-hps", "Ya");
    }
  });
}

function dateFormater(date) {
  return new Intl.DateTimeFormat('en-GB', { dateStyle: 'medium', timeStyle: 'short'}).format(new Date(date));
}

function hideModal() {
  $('#modal-delete').modal('hide');
}

function toggleSidebar() {
  if (sidebar == "1") {
    closeSidebar();
  } else {
    openSidebar();
  }
}

function closeSidebar() {
  $("#nav-bar").css("width", "4.5rem");
  $(".main-content").css("margin-left", "-11rem");
  $("#header").css("margin-left", "-11rem");
  $("#header").css("width", "115%");
  sidebar = "0";
}

function openSidebar() {
  $("#nav-bar").css("width", "250px");
  $(".main-content").css("margin-left", "0");
  $("#header").css("margin-left", "0");
  $("#header").css("width", "100%");
  sidebar = "1";
}

function redirectToEdit(id) {
  localStorage.setItem("edit_id", JSON.stringify(id));
  window.location.href = "/cars/edit";
}

function size(id) {
  switch (id) {
    case "1":
      return "Small";
    case "2":
      return "Medium";
    case "3":
      return "Large";
  }
}

function DisableBtn(selector) {
  $(selector).prop("disabled", true);
  $(selector).text("Tunggu ...");
}

function EnableBtn(selector, defaultText) {
  $(selector).prop("disabled", false);
  $(selector).text(defaultText);
}

function pesanAlert(obj, pos = "center") {
  let type = "";
  let color = "";
  let msg = "";
  switch (parseInt(obj.code)) {
      case 201:
          type = "success";
          color = "#73CA5C";
          msg = obj.message;
          break;
      case 202:
          type = "success";
          color = "#000";
          msg = obj.message;
          break;
      default:
          type = "error";
          color = "red";
          msg = obj.message;
          break;
  }
  notif({
      msg: '<b style="color: white;">' + msg + '</b>',
      type: type,
      bgcolor: color,
      position: pos
  });
}