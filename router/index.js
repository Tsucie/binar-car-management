const router = require("express").Router();
const { Car } = require("../models");

// Define route modules here
router.use("/api/car", require("./car"));

// Define route views here
router.get("/", (req, res) => {
  res.render("index.ejs", { title: "Dashboard | BCM" });
});
router.get("/cars", (req, res) => {
  res.render("car/cars.ejs", { title: "Cars | BCM" });
});

// * Rizal
router.get("/cars/add", (req, res) => {
  res.render("car/addcar.ejs", { title: "Add Cars | BCM" });
});
// * Rizal

router.get("/cars/edit", (req, res) => {
  res.render("car/editcar.ejs", { title: "Edit Car | BCM" });
});

module.exports = router;
