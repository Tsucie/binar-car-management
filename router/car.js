const router = require("express").Router();
const upload = require("../utilities/uploadMiddleware");
const CarController = require("../controller/carController");

// REST API ROUTES

/**
 * HTTP POST "/api/car/getAll"
 * return json
 */
router.post("/getAll", CarController.getAll);

/**
 * HTTP POST "/api/car/getById"
 * return json
 */
router.post("/getById", CarController.getCarByID);

/**
 * HTTP POST "/api/car/add"
 * return json
 */
router.post("/add", upload.single("photo"), CarController.add);

/**
 * HTTP PUT "/api/car/edit"
 * return json
 */
router.put("/edit", upload.single("photo"), CarController.edit);

/**
 * HTTP DELETE "/api/car/delete"
 * return json
 */
router.delete("/delete", CarController.delete);

module.exports = router;
