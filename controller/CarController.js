const path = require("path");
const fs = require("fs");
const Resizer = require("../utilities/Resizer");
const { Car } = require("../models");

// REST API Controller
class CarController {
  static async getAll(req, res) {
    try {
      let whereClause = {};
      let { filter } = req.body;
      if (filter) {
        whereClause = {
          where: {
            size_id: req.body.filter,
          },
        };
      }
      let cars = await Car.findAll(whereClause);
      return res.status(200).json({ code: 200, message: "OK", data: cars });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        code: 500,
        message: "Internal Server Error",
      });
    }
  }
  static async getCarByID(req, res) {
    try {
      let { id } = req.body;
      let car = await Car.findByPk(id);
      return res.status(200).json({ code: 200, message: "OK", data: car });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        code: 500,
        message: "Internal Server Error",
      });
    }
  }
  static async add(req, res) {
    try {
      // Validasi Form
      if (!req.body.name || !req.body.price || !req.body.size_id) {
        return res.status(400).json({ code: 400, message: "Please fill the data form!" });
      }
      // Proses gambar
      if (req.file) {
        const imgPath = path.join(__dirname, "/../public/img");
        const fileUpload = new Resizer(imgPath, `${req.body.name}_`);
        const filename = await fileUpload.save(req.file.buffer);
        req.body.photo = filename;
      }
      // Insert Data
      Car.create(req.body)
        .then((car) => {
          res
            .status(201)
            .json({ code: 201, message: "Data Berhasil Disimpan", data: car });
        })
        .catch((error) => {
          throw error;
        });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        code: 500,
        message: "Internal Server Error",
      });
    }
  }
  static async edit(req, res) {
    try {
      let { id, size_id, name, price, photo } = req.body;
      // Cari data berdasarkan id
      let car = await Car.findByPk(id);
      // Data tidak ditemukan
      if (!car) {
        return res.status(404).json({ code: 404, message: "No Data" });
      }
      let updateData = new Object();
      if (size_id) updateData.size_id = size_id;
      if (name) updateData.name = name;
      if (price) updateData.price = price;
      // Jika user upload photo
      if (req.file) {
        const imgPath = path.join(__dirname, "/../public/img");
        const fileUpload = new Resizer(imgPath, `${name}_`);
        const filename = await fileUpload.save(req.file.buffer);
        updateData.photo = filename;
        // Hapus photo lama
        fs.unlinkSync(path.join(imgPath, car.photo));
      }
      // Update data
      await Car.update(updateData, {
        where: {
          id: id,
        },
      });

      return res
        .status(201)
        .json({ code: 201, message: "Data Berhasil Diubah" });
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        code: 500,
        message: "Internal Server Error",
      });
    }
  }
  static async delete(req, res) {
    try {
      let { id } = req.body;
      console.log(id);
      let car = await Car.findByPk(id);
      if (car) {
        let imgPath = path.join(__dirname, "/../public/img/", car.photo);
        fs.unlink(imgPath, (err) => {
          if (err) throw err;
          Car.destroy({
            where: {
              id: car.id
            }
          }).then(() => {
            res.status(202).json({ code: 202, message: "Data Berhasil Dihapus"});
          }).catch((error) => {
            throw error;
          });
        });
      }
      else {
        return res.status(400).json({ code: 400, message: "No Data" });
      }
    } catch (error) {
      console.error(error);
      return res.status(500).json({
        code: 500,
        message: "Internal Server Error",
      });
    }
  }
}

module.exports = CarController;
