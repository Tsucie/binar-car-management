const { Sequelize } = require('sequelize');
const config = require('./appconfig');

class DB {
    constructor() {
        this.connStr = config.db.conn;
        this.dbName = config.db.dbName;
        this.dbContext = new Sequelize(this.connStr);
    }

    async connect() {
        try {
            await this.dbContext.authenticate();
            console.log('Connection has been established successfully.');
        } catch (error) {
            console.error('Unable to connect to the database:', error);
        }
    }
}

module.exports = DB;