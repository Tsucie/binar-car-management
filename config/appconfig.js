require("dotenv").config();

module.exports = {
  app: {
    port: process.env.DEV_APP_PORT || 8000,
    appName: process.env.APP_NAME || "binar-car-management",
    env: process.env.NODE_ENV || "development",
  },
  db: {
    conn: process.env.PG_CONNECTION || "postgres://nazrshlm:R5_mm0STfWOgnVkmEnS0RBWTZwas5k8H@rosie.db.elephantsql.com/nazrshlm",
    dbName: process.env.PG_DATABASE || "nazrshlm",
  },
};
