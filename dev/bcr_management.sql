CREATE TABLE "size" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar
);

CREATE TABLE "cars" (
  "id" int PRIMARY KEY,
  "id_size" int,
  "name" varchar,
  "price" decimal,
  "photo" varchar,
  "created_at" datetime,
  "updated_at" datetime
);

ALTER TABLE "cars" ADD FOREIGN KEY ("id_size") REFERENCES "size" ("id");
