"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Adji
     */
    return await queryInterface.bulkInsert(
      "Sizes",
      [
        {
          id: 1,
          name: "Small",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 2,
          name: "Medium",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: 3,
          name: "Large",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {}
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Adji
     */
    return await queryInterface.bulkDelete("Sizes", null, {});
  },
};
